1. Baixar o projeto para testar usando o comando "git clone https://guimachadoo@bitbucket.org/guimachadoo/bdr.git";

2. Conexão com o banco de dados - Verificar se usuário e senha do seu localhost está igual ao do código em config/app.php;

3. Banco de dados criado e com informações, basta importar o arquivo .sql localizado em /config/cake_api.sql;

4. No exercício 4 são encontrados usuários, livros e tags para cada livro. É possível buscar tags existentes na url /bookmarks/tagged/tagdesejada.
Como exemplo pode utilizar /bookmarks/tagged/teste. "tagged" é a função que busca tags com a palavra "teste".

Para acessar, basta criar uma conta com e-mail e senha. A partir daí pode-se cadastrar novos livros, tags e usuários. Essa aplicação foi feita com
CakePHP, portanto será necessário que esteja instalado o composer, PHP 5.6.0 ou maior, habilitar as extensões mbstring e intl.

A aplicação pode ser testada utilizando o comando bin/cake server (linux) ou bin\cake server(windows). Ficará disponível no endereço localhost:8765.
