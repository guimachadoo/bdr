<div class="row"><br>
    <div class="columns large-12 medium-12">
        <?php
            for($i=1; $i<=100; $i++) {
                if( $i%3 == 0 ) echo "Fizz";
                if( $i%5 == 0 ) echo "Buzz";
                if( ($i%3 != 0) and ($i%5 != 0) ) echo $i;
                if($i == 100){
                    echo ".";
                }
                else{
                    echo ", ";
                }
            }
        ?>
    </div>
</div>