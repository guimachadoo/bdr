<div class="row"><br>
    <div class="columns large-6">
        <?php
            echo $this->Html->image("/img/print-1.png", ["alt" => "Session"]);
        ?>
    </div>

    <div class="columns large-6">
        1. Importante ressaltar o uso de session_start();<br>
        2. O uso de ISSET só serve para verificar se a variável foi definida ou não, e nesse caso já é verificado se o
        parâmetro é true;<br>
        3. Adicionei o trecho sobre cookies dentro do else da SESSION. Caso a sessão não exista, busca pelo cookie ativo,
        e se não houver, emite a mensagem de retorno.
    </div>
</div>
