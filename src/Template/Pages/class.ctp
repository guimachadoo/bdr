<div class="row"><br>
    <div class="columns large-9">
        <?php
            echo $this->Html->image("/img/print-2.png", ["alt" => "Conexão com o banco de dados"]);
        ?>
    </div>

    <div class="columns large-3">
        Classe de conexão com o banco de dados.
    </div>
</div>

<div class="row" style="margin-top: 2.5em"></div>

<div class="row">
    <div class="columns large-5">
        Classe, parâmetros e função que retorna os usuários.
    </div>

    <div class="columns large-7 text-right">
        <?php
            echo $this->Html->image("/img/print-3.png", ["alt" => "Session"]);
        ?>
    </div>
</div>