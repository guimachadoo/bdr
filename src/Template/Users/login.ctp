<div class="users form large-5 medium-5 columns content">
    <h1>Login</h1>
    <?= $this->Form->create() ?>
    <?= $this->Form->input('email') ?>
    <?= $this->Form->input('password') ?>
    <?= $this->Form->button('Login') ?>
    <?= $this->Form->end() ?>

    <?php echo $this->Html->link('Criar conta', ['controller' => 'Users', 'action' => 'add']); ?>
</div>
