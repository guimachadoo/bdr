-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 19, 2017 at 06:02 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cake_api`
--

-- --------------------------------------------------------

--
-- Table structure for table `bookmarks`
--

CREATE TABLE IF NOT EXISTS `bookmarks` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `description` text,
  `url` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bookmarks`
--

INSERT INTO `bookmarks` (`id`, `user_id`, `title`, `description`, `url`, `created`, `modified`) VALUES
(1, 1, 'Livro Teste 01', 'Lorem ipsum dor sit amet', '', '2017-08-18 22:04:32', '2017-08-18 22:04:45'),
(2, 1, 'Livro 02 Teste', 'Lorem ipsum dor sit amet', '', '2017-08-18 22:05:00', '2017-08-18 22:05:00'),
(3, 1, 'Livro 3333 Teste', 'Lorem ipsum dor sit amet', '', '2017-08-18 22:05:17', '2017-08-18 22:05:17'),
(4, 1, 'Teste 04', 'Teste', '', '2017-08-18 23:26:24', '2017-08-18 23:26:24'),
(5, 1, 'Teste 06', 'TEste', '', '2017-08-18 23:28:36', '2017-08-18 23:28:36'),
(6, 1, '007 Bookmark', 'Book teste 07', '', '2017-08-19 03:28:45', '2017-08-19 03:29:45');

-- --------------------------------------------------------

--
-- Table structure for table `bookmarks_tags`
--

CREATE TABLE IF NOT EXISTS `bookmarks_tags` (
  `bookmark_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bookmarks_tags`
--

INSERT INTO `bookmarks_tags` (`bookmark_id`, `tag_id`) VALUES
(2, 1),
(3, 2),
(1, 3),
(4, 4),
(6, 4),
(4, 5),
(5, 5),
(6, 7);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `title`, `created`, `modified`) VALUES
(1, 'Tag 01', '2017-08-18 22:02:21', '2017-08-18 22:02:21'),
(2, 'Tag 02', '2017-08-18 22:02:39', '2017-08-18 22:02:39'),
(3, 'Tag 03', '2017-08-18 22:03:08', '2017-08-18 22:03:08'),
(4, 'Teste', '2017-08-18 23:26:24', '2017-08-18 23:26:24'),
(5, 'Teste 05', '2017-08-18 23:28:20', '2017-08-18 23:28:20'),
(7, 'Tag 007', '2017-08-19 03:30:39', '2017-08-19 03:30:39');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `created`, `modified`) VALUES
(1, 'gui@teste.com', '$2y$10$2iCkLGKIk9gKpv1F0slB..zCovMFZiWZJxSC.A2rh25n1kjgzdUL2', '2017-08-18 22:01:34', '2017-08-18 22:09:34'),
(2, 'teste@gmail.com', '$2y$10$V61dC90sXr7B/jHkULsTIuql0/4QG7PEvxrpLqxWsXaQpWsQWO6Wq', '2017-08-19 02:08:07', '2017-08-19 02:08:07'),
(3, 'abc@gmail.com', '$2y$10$Slxv1dWDD7Je11rELkX6dOVTwAbJsknpxz6x9AbpIvm6Q5MncifNi', '2017-08-19 02:16:41', '2017-08-19 02:16:41'),
(4, 'lorem@teste.com', '$2y$10$TBIg2bkhTTkquj4oZNW8teqiSwp57ltFNVu3t9kLmGUlg.LEBj/9C', '2017-08-19 02:46:03', '2017-08-19 02:46:03'),
(5, 'teste@teste.com', '$2y$10$t2MkBDsmf6Lysr/RoiOXaeYGG6EMDk0UH0xmHw0UF7gKnQmg.teo2', '2017-08-19 02:47:16', '2017-08-19 02:47:16'),
(6, 'teste2@teste.com', '$2y$10$y0xXJ0oPbWxyqS0OBUVNnOUA2lP4EnyKACqhzROoJ7hMdsPBL3xpm', '2017-08-19 02:47:33', '2017-08-19 02:47:33'),
(7, 'def@gmail.com', '$2y$10$/aMXbJJ7zUzQFrxyzsTOh.64ZOkujZ801Zxcm2iCjwt3SJOFub6HK', '2017-08-19 02:49:05', '2017-08-19 02:49:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bookmarks`
--
ALTER TABLE `bookmarks`
  ADD PRIMARY KEY (`id`), ADD KEY `user_key` (`user_id`);

--
-- Indexes for table `bookmarks_tags`
--
ALTER TABLE `bookmarks_tags`
  ADD PRIMARY KEY (`bookmark_id`,`tag_id`), ADD KEY `tag_key` (`tag_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `title` (`title`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bookmarks`
--
ALTER TABLE `bookmarks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `bookmarks`
--
ALTER TABLE `bookmarks`
ADD CONSTRAINT `bookmarks_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `bookmarks_tags`
--
ALTER TABLE `bookmarks_tags`
ADD CONSTRAINT `bookmarks_tags_ibfk_1` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`),
ADD CONSTRAINT `bookmarks_tags_ibfk_2` FOREIGN KEY (`bookmark_id`) REFERENCES `bookmarks` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
